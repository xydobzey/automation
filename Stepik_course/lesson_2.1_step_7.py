import time

from selenium import webdriver
import math


def calc(x):
    return str(math.log(abs(12*math.sin(int(x)))))


driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.get('http://suninjuly.github.io/math.html')
element = driver.find_element_by_xpath('//*[@id="input_value"]').text
result = calc(element)
field = driver.find_element_by_xpath('//*[@id="answer"]').send_keys(result)
a = driver.find_element_by_xpath('//*[@id="robotCheckbox"]').click()
b = driver.find_element_by_xpath('//*[@id="robotsRule"]').click()
c = driver.find_element_by_xpath('/html/body/div/form/button').click()
time.sleep(30)
driver.quit()
