import time
from selenium import webdriver
import math

def calc(x):
    return str(math.log(abs(12*math.sin(int(x)))))
driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.get('http://suninjuly.github.io/alert_accept.html')
driver.find_element_by_xpath('//form/div/div/button').click()
confirm = driver.switch_to.alert.accept()
element = driver.find_element_by_xpath('//*[@id="input_value"]').text
result = calc(element)
field = driver.find_element_by_xpath('//*[@id="answer"]').send_keys(result)
driver.find_element_by_xpath('/html/body/form/div/div/button').click()
time.sleep(5)
driver.quit()
