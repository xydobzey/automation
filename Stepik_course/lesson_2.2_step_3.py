import time
from selenium import webdriver
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.get('http://suninjuly.github.io/selects1.html')
num = driver.find_element_by_xpath('//*[@id="num1"]').text
num2 = driver.find_element_by_xpath('//*[@id="num2"]').text
result = int(num) + int(num2)
print(result)
select = Select(driver.find_element_by_xpath('//*[@id="dropdown"]'))
select.select_by_value(str(result))
driver.find_element_by_xpath('//form/button').click()
time.sleep(5)
driver.quit()
