from selenium import webdriver
import time
import unittest
class TestAbs(unittest.TestCase):
    def test_01(self):
        link = "http://suninjuly.github.io/registration1.html"
        link2 = "http://suninjuly.github.io/registration2.html"
        self.browser = webdriver.Chrome()
        self.browser.get(link)

        # Ваш код, который заполняет обязательные поля
        self.browser.find_element_by_xpath('//form/div[1]/div[1]/input').send_keys('vvv')
        self.browser.find_element_by_xpath('//form/div[1]/div[2]/input').send_keys('rrr')
        self.browser.find_element_by_xpath('//form/div[1]/div[3]/input').send_keys('wwyandex.ru')

        # Отправляем заполненную форму
        button = self.browser.find_element_by_css_selector("button.btn")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(10)

        # находим элемент, содержащий текст
        welcome_text_elt =  self.browser.find_element_by_tag_name("h1")
        text="Congratulations! You have successfully registered!"
        # записываем в переменную welcome_text текст из элемента welcome_text_elt
        welcome_text = welcome_text_elt.text
        self.assertEqual(text, welcome_text)
        # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта

        # ожидание чтобы визуально оценить результаты прохождения скрипта


        # закрываем браузер после всех манипуляций
        self.browser.quit()
    def test_02(self):
        link = "http://suninjuly.github.io/registration1.html"
        link2 = "http://suninjuly.github.io/registration2.html"
        self.browser = webdriver.Chrome()
        self.browser.get(link2)

        # Ваш код, который заполняет обязательные поля
        self.browser.find_element_by_xpath('//form/div[1]/div[1]/input').send_keys('vvv')
        self.browser.find_element_by_xpath('//form/div[1]/div[2]/input').send_keys('rrr')
        self.browser.find_element_by_xpath('//form/div[1]/div[3]/input').send_keys('wwyandex.ru')

        # Отправляем заполненную форму
        button = self.browser.find_element_by_css_selector("button.btn")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(10)

        # находим элемент, содержащий текст
        welcome_text_elt =  self.browser.find_element_by_tag_name("h1")
        text="Congratulations! You have successfully registered!"
        # записываем в переменную welcome_text текст из элемента welcome_text_elt
        welcome_text = welcome_text_elt.text
        self.assertEqual(text, welcome_text)
        # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта

        # ожидание чтобы визуально оценить результаты прохождения скрипта


        # закрываем браузер после всех манипуляций
        self.browser.quit()
if __name__ == "__main__":
    unittest.main()