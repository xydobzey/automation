import time

from selenium import webdriver
import math


def calc(x):
    return str(math.log(abs(12*math.sin(int(x)))))


driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.get('http://suninjuly.github.io/get_attribute.html')
treasure = driver.find_element_by_xpath('//*[@id="treasure"]').get_attribute('valuex')
result = calc(treasure)
field = driver.find_element_by_xpath('//*[@id="answer"]').send_keys(result)
a = driver.find_element_by_xpath('//*[@id="robotCheckbox"]').click()
b = driver.find_element_by_xpath('//*[@id="robotsRule"]').click()
c = driver.find_element_by_xpath('//form/div/div/button').click()

time.sleep(5)
driver.quit()
