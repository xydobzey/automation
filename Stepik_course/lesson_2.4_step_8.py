import time
from selenium import webdriver
import math

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def calc(x):
    return str(math.log(abs(12*math.sin(int(x)))))
driver = webdriver.Chrome()
# driver.implicitly_wait(5)
driver.get('http://suninjuly.github.io/explicit_wait2.html')
wait = WebDriverWait(driver, 12)
price = wait.until(EC.text_to_be_present_in_element((By.ID, 'price'), '100'))
driver.find_element_by_xpath('//*[@id="book"]').click()

element = driver.find_element_by_xpath('//*[@id="input_value"]').text
result = calc(element)
field = driver.find_element_by_xpath('//*[@id="answer"]').send_keys(result)
driver.find_element_by_xpath('/html/body/form/div/div/button').click()
time.sleep(5)
driver.quit()