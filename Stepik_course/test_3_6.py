import time
import math
import pytest
from selenium import webdriver



@pytest.fixture
def driver():
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


with open('data.txt', 'r') as f:
    url = f.read().split('\n')


@pytest.mark.parametrize('url', url)
def test(driver, url):
    answer = math.log(int(time.time()))
    driver.get(url)
    driver.find_element_by_xpath("//div/textarea").send_keys(f'{answer}')
    driver.find_element_by_xpath("//div/button").click()
    correct = driver.find_element_by_xpath("//div/pre[@class='smart-hints__hint']").text
    assert correct == "Correct!"
    time.sleep(3)
