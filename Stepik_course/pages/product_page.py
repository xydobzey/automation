from .base_page import BasePage
from .locators import PageProductsLocators


class PageProduct(BasePage):
    def add_basket(self):
        add_button = self.browser.find_element(*PageProductsLocators.ADD_BUTTON)
        add_button.click()

    def should_add_book_in_basket(self):
        ex_name = self.browser.find_element(*PageProductsLocators.EX_NAME).text
        ac_name = self.browser.find_element(*PageProductsLocators.AC_NAME).text

        assert ex_name == ac_name, 'Name product does not match the value in the basket'

    def should_price_in_basket(self):
        ex_price = self.browser.find_element(*PageProductsLocators.EX_PRICE).text
        ac_price = self.browser.find_element(*PageProductsLocators.AC_PRICE).text
        assert ex_price == ac_price, 'price does not match the value in the basket'

    def should_not_be_success_message(self):
        assert self.is_not_element_present(*PageProductsLocators.SUCCESS_MESSAGE), "Success message is presented, but should not be"

    def should_is_disappeared_message(self):
        assert self.is_disappeared(*PageProductsLocators.SUCCESS_MESSAGE), "Success message is presented, but should not be"
