from selenium.webdriver.common.by import By


class MainPageLocators():
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")


class LoginPageLocators():
    LOGIN_FORM = (By.XPATH, '//*[@id="login_form"]')
    REGISTER_FORM = (By.XPATH, '//*[@id="register_form"]')


class PageProductsLocators():
    ADD_BUTTON = (By.CSS_SELECTOR, '#add_to_basket_form > button')
    EX_NAME = (By.XPATH, '//*[@id="content_inner"]/article/div[1]/div[2]/h1')
    AC_NAME = (By.XPATH, '//*[@id="messages"]/div[1]/div/strong')
    EX_PRICE = (By.XPATH, '//*[@id="content_inner"]/article/div[1]/div[2]/p[1]')
    AC_PRICE = (By.XPATH, '//*[@id="messages"]/div[3]/div/p[1]/strong')
    SUCCESS_MESSAGE = (By.CSS_SELECTOR, '#messages > div:nth-child(2) > div')


