import pytest
from selenium.webdriver.chrome.webdriver import WebDriver


@pytest.fixture
def driver():
    driver = WebDriver(executable_path='D://selenium//chromedriver.exe')
    driver.implicitly_wait(5)
    yield driver
    driver.quit()
