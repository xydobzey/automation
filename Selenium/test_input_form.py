from selenium.webdriver.chrome.webdriver import WebDriver


def test_input(driver: WebDriver, message='Python'):
    driver.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')
    input_text = driver.find_element_by_xpath('//input[@type="text"][@id="user-message"]')
    input_text.clear()
    input_text.send_keys(message)
    driver.find_element_by_xpath('//form[@id="get-input"]//button').click()
    message_display = driver.find_element_by_xpath('//span[@id="display"]')
    assert message == message_display.text
    print(message)


def test_sum_a_b(driver: WebDriver, a=5, b=10):
    driver.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')
    enter_a = driver.find_element_by_xpath('//input[@id="sum1"]')
    enter_a.send_keys(a)
    enter_b = driver.find_element_by_xpath('//input[@id="sum2"]')
    enter_b.send_keys(b)
    driver.find_element_by_xpath('//form[@id="gettotal"]//button').click()
    message_display = driver.find_element_by_xpath('//span[@id="displayvalue"]')
    assert str(a + b) == message_display.text
    print(str(a + b))


def test_single_checkbox(driver: WebDriver):
    driver.get('https://www.seleniumeasy.com/test/basic-checkbox-demo.html')
    check_box = driver.find_element_by_xpath('//input[@id="isAgeSelected"]')
    check_box.click()
    assert check_box.is_selected() is True
    message = driver.find_element_by_xpath('//div[@id="txtAge"]')
    assert 'Success - Check box is checked' == message.text
    print(message.text)
