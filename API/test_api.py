import jsonpath
import requests
import pytest
import json



# def test_api_list_users():
url = 'https://reqres.in/api/users?page=2'
response = requests.get(url)
# assert response.status_code == 200
# print(response.content)
# print(response.headers)
json_response = json.loads(response.text)
print(json.dumps(json_response))

# page = jsonpath.jsonpath(json_response, 'total_pages')
# print(page)
# print(page[0])
# assert page[0] == 2
with open('../API/data_get/data_users.json', 'w', encoding='utf-8') as file:
    json.dump(json_response, file, indent=4)
